Install and run
===============

1. Install deps: `pip3 install --user gensim flask httpx ijson dataclasses`
2. Download GloVe data: `curl http://downloads.cs.stanford.edu/nlp/data/glove.840B.300d.zip -o glove.840B.300d.zip && unzip glove.840B.300d.zip`
3. Preprocess GloVe data: `./prep_word2vec.py`
4. Run server: `PORT=3335 ./server.py`

Load a dataset
==============

1. Download `overview-server` and `./dev`; or download `overview-local` and run `./start`
2. Browse to http://localhost:9000. Create a document set.
3. Create a View. Name it `localhost:3335` and point it to `http://localhost:3335`
4. Inspect it in the browser pane. Look at the URL of the `<iframe>`.

The URL has `?server=...&documentSetId=...&apiToken=...`

Now you can generate a dataset. Fill in the correct `documentSetId` and `apiToken` here:

`curl --no-buffer -v "http://localhost:3335/generate" --data "server=http://localhost:9000&documentSetId=2" --user 206bjk78o5qr2cq81ricskgwg:x-auth-token`

This will stream JSON progress events.

And you can search the output:

`curl --no-buffer -v "http://localhost:3335/search?term=first+second&server=http://localhost:9000&documentSetId=2" --user 206bjk78o5qr2cq81ricskgwg:x-auth-token

This will output JSON data with "similarTokens" objects and "foundTokens"

Test WorkQueue
==============

WorkQueue should be a separate Python package. In the meantime:

`PYTHONPATH=. pytest -v`

TODO
====

* [adam] `.workqueue`: delete tempfiles on startup.
