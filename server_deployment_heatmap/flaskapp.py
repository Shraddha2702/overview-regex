import base64
import functools
import itertools
import json
import re
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path
from typing import Any, Dict, Mapping

import flask
from flask import current_app, jsonify, request
from werkzeug.exceptions import BadRequest, NotFound
from werkzeug.wrappers import Response
from workqueue import OverviewViewParams, WorkQueue


from urllib.parse import urlparse
from indexfile import DataFile

import sqlite3

__all__ = ["app"]


_AUTH_HEADER_REGEX = re.compile(r"^Basic ([+/a-zA-Z0-9]+={0,3})$")
_AUTH_TOKEN_STRING_REGEX = re.compile(rb"^([^:]+):x-auth-token$")


app = flask.Flask("heatmap-demo")
app.config["DEBUG"] = True


WORK_QUEUE = WorkQueue(
    Path(__file__).parent / "generate_inverted_index.py",
    executor=ThreadPoolExecutor(1, "heatmap-index-generator"),
    storage_dir=Path(__file__).parent / "heatmaps",
)


@app.after_request
def _add_cors_headers(response):
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type"
    return response


def _extract_api_token(auth_header: str) -> str:
    m = _AUTH_HEADER_REGEX.match(auth_header)
    if not m:
        raise ValueError(
            'Authorization header must look like "Basic [base64-encoded data]"'
        )
    decoded = base64.b64decode(m.group(1))
    m = _AUTH_TOKEN_STRING_REGEX.match(decoded)
    if not m:
        raise ValueError(
            'base64-encoded Basic HTTP Auth value must look like "[api_token]:x-api-token"'
        )
    return m.group(1).decode("latin1")


def _extract_server_url(url: str) -> str:
    u = urlparse(url)
    if not u.scheme or not u.netloc or u.params or u.query or u.fragment:
        raise ValueError(
            "'server' must look like 'http://domain' or 'https://domain/...'; got %r"
            % url
        )
    return url


def _extract_params(
    form: Mapping[str, str], headers: Mapping[str, str]
) -> OverviewViewParams:
    try:
        return OverviewViewParams(
            _extract_server_url(form["server"]),
            form["documentSetId"],
            _extract_api_token(headers["Authorization"]),
        )
    except (KeyError, ValueError) as err:
        raise BadRequest(
            "Expected form data ?server=...&documentSetId=... and Authorization header. "
            "Problem: %s" % str(err)
        )


@app.route("/show", methods=["GET"])
def show():
    return "This should serve a React app."


@app.route("/metadata", methods=["GET"])
def metadata():
    return jsonify({})


@app.route("/generate", methods=["POST", "OPTIONS"])
def generate():
    if request.method == "OPTIONS":
        response = current_app.make_default_options_response()
        response.headers["Access-Control-Allow-Methods"] = "POST, OPTIONS"
        response.headers["Access-Control-Max-Age"] = "300"
        return response

    #print(request.get_json())
    overview_params = _extract_params(request.get_json(), request.headers)

    job = WORK_QUEUE.ensure_run(overview_params)
    if job is None:
        # The work is done; the model can be loaded now.
        return Response(status=204)  # No Content -- no need to do anything
    else:
        # Stream progress to the user, as a JSON Array. (Must not be buffered.)
        progress_stream = WORK_QUEUE.report_job_progress_until_completed(job)
        json_stream = itertools.chain(
            ["["],
            (
                # JSON array: comma between elements, not before first or after last
                ("" if i == 0 else ",") + json.dumps(progress._asdict())
                for i, progress in enumerate(progress_stream)
            ),
            ["]"],
        )

        return Response(json_stream, content_type="application/json")


# CALLS - _load_precomputed_index - and returns fetching the data
@app.route("/search", methods=["GET"])
def search():
    if request.method == "OPTIONS":
        response = current_app.make_default_options_response()
        response.headers["Access-Control-Allow-Methods"] = "GET, OPTIONS"
        response.headers["Access-Control-Max-Age"] = "300"
        return response

    overview_params = _extract_params(
        request.args, request.headers
    )  # raises BadRequest
    try:
        tokens = request.args["term"].lower().split(" ")  # TODO tokenize
    except KeyError as err:
        raise BadRequest(
            "Expected QueryString with ?...&term=.... Problem: %r" % str(err)
        )

    if (len(tokens) != 1):
        raise BadRequest("Term must be 1 token")

    path = WORK_QUEUE.destination_path_for_params(overview_params)

    try:
        #word_stats_map = _load_precomputed_index(overview_params)
        datafile = DataFile(path)
        # [count1, count2, ...] -> indexed by docid
        doc_counts = datafile.lookup_token(tokens[0])
    except sqlite3.OperationalError as err:
        raise NotFound(
            "No file found at %r. Please POST to (and wait for) /generate."
            % path
        )

    if doc_counts is None:
        raise NotFound("No document contains this Token")
    
    useful_doc_ids = [doc_id for doc_id,
                      count in enumerate(doc_counts) if count > 0]
    documents = datafile.get_documents(useful_doc_ids)

    found_entries = [
        dict(id=doc.id, title=doc.title, pageNumber=doc.pageNumber, count=doc_counts[doc.id], maxPageNumber=doc.maxPageNumber)
        for doc in documents
    ]
    
    return jsonify({"foundEntries": found_entries})