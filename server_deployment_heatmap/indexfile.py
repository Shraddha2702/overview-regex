import array
import sqlite3
from collections import namedtuple
from typing import Dict, List
DocumentInfo = namedtuple(
    'DocumentInfo', ['id', 'title', 'pageNumber', 'maxPageNumber'])


def encode_document_token_counts(counts: List[int]) -> bytes:
    """Encode token counts for storage in a database.
    Input: List mapping document-ID to token count. For instance,
    `[3, 0, 1]` means, "doc0 has 3 instances, doc1 has 0 instances,
    doc2 has 1 instance".
    Output: bytes, intended to be passed to `decode_document_token_counts()`.
    """
    return array.array('i', counts).tobytes()


def decode_document_token_counts(encoded: bytes) -> List[int]:
    return array.array('i', encoded)


class DataFile:
    def __init__(self, path):
        self.conn = sqlite3.connect(path)

    def get_documents(self, ids):
        if not ids:
            return []
        cursor = self.conn.cursor()
        cursor.execute("SELECT id, title, pageNumber, maxPageNumber FROM documents WHERE id IN (%s) ORDER BY position" % ','.join(
            str(i) for i in ids))
        return [DocumentInfo(*row) for row in cursor.fetchall()]

    def lookup_token(self, token):
        cursor = self.conn.cursor()
        cursor.execute("SELECT encoded_counts FROM token_doc_counts WHERE token = :token", {
                       "token": token})
        row = cursor.fetchone()
        if row is None:
            return None
        else:
            document_token_counts_bytes = row[0]
            return decode_document_token_counts(document_token_counts_bytes)

    @classmethod
    def create(cls, path, documents: List[DocumentInfo], token_counts: Dict[str, List[int]]):
        conn = sqlite3.connect(path)
        cursor = conn.cursor()
        cursor.execute(
            'CREATE TABLE documents (id INT PRIMARY KEY, title VARCHAR, pageNumber INT, maxPageNumber INT, position INT)')
        cursor.executemany(
            'INSERT INTO documents (id, title, pageNumber, maxPageNumber, position) VALUES (?, ?, ?, ?, ?)',
            (( doc.id, doc.title, doc.pageNumber, doc.maxPageNumber, position )
                for position, doc in enumerate(documents) )
        )
        cursor.execute(
            'CREATE TABLE token_doc_counts (token VARCHAR PRIMARY KEY, encoded_counts BLOB)')
        cursor.executemany('INSERT INTO token_doc_counts (token, encoded_counts) VALUES (?, ?)', ((
            token, encode_document_token_counts(counts)) for token, counts in token_counts.items()))
        conn.commit()
        return cls(path)
