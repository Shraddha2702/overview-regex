import os

from flaskapp import app

if __name__ == "__main__":
    try:
        port = int(os.environ["PORT"])
    except (KeyError, ValueError):
        port = 3334
    app.run(host="127.0.0.1", port=port, debug=True)
