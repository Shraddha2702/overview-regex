#!/usr/bin/env python3
from pathlib import Path
from typing import BinaryIO, ContextManager, Iterable, List, NamedTuple

import json
import logging

import array
from indexfile import DataFile, DocumentInfo

from collections import namedtuple
from typing import Dict, List

import contextlib
import tempfile
import httpx
import ijson


# GLOVE_W2V_BIN_PATH = Path(__file__).parent / "glove.840B.300d.w2v.gensim.bin"
class OverviewViewParams(NamedTuple):
    server: str
    """URL for Overview API server. e.g.: `https://www.overviewdocs.com`"""

    document_set_id: str
    """DocumentSet ID on Overview API server."""

    api_token: str
    """Token granting access to document set on Overview API server."""




# generate_index - load_sentence_stream (Entire Doc Set -> Page Wise) - _stream_json_bytes_into_file

#OUTPUT - WRITES TO THE FILE - docName, api params -> Get Text of the page - stores in Temp File
def _stream_json_bytes_into_file(params: OverviewViewParams, w: BinaryIO) -> None:
    """Make an HTTPX request and write to the given file-like object."""
    with httpx.stream(
        "GET",
        f"{params.server}/api/v1/document-sets/{params.document_set_id}/documents", #SPECIFY DOCID, PAGENO
        params=dict(stream=True, fields="tokens,title,pageNumber"), #MODIFY Fields - Tokens, Title 
        auth=(params.api_token, "x-auth-token"),  #Write to File ----> Doc, PageNo, Tokens
    ) as response:
        response.raise_for_status()  # require 200 OK
        for chunk in response.iter_bytes():
            w.write(chunk) ###################### MODIFY





#GIVES ALL THE KEYWORDS
@contextlib.contextmanager
def load_documents_stream(
  params: OverviewViewParams,
) -> ContextManager[Iterable[List[str]]]:
  """
  Preload documents; return a stream of List[str] tokens.
  Usage:
      with load_sentence_stream(params) as sentence_stream:
          for doc: List[str] in sentence_stream:
              yield doc  # or whatever
  """
  with tempfile.TemporaryFile() as tf:
      _stream_json_bytes_into_file(params, tf)
      tf.seek(0)
      class TempfileJsonStreamer(NamedTuple):
          f: BinaryIO
          def __iter__(self):
              for doc in ijson.items(self.f, "items.item"):
                  yield doc
              self.f.seek(0)
      yield TempfileJsonStreamer(tf)



#RETURNS INVERTED INDEX - Input: text each page -> Name of page
#Input - To fill up (Document id, Page title, page Number, maxPageNumber) 
#Input: id = Document Set id
def generate_index(params: OverviewViewParams, path: Path) -> None:
    ## TODO: LOADING FUNCTION !
    #EXTRACT DOCUMENT AND TOKENS FROM DICTIONARY ----------------------------------
    documents = [] # List of indexfile.Document(id, title, pageNumber, maxPageNumber)
    #Token -> List of Counts indexed by Doc ID
    token_counts = {}
    max_doc_id = 0
    doc_page_counts = {} #Page number indexed by doc id
    with load_documents_stream(params) as document_stream:
        for doc in document_stream:
            #doc ? -> {id: int, title: "", pageNumber: null|int, tokens: ""}
            doc_id = doc['id'] & 0xffffffff #We only care about 32-bit document ids
            documents.append(DocumentInfo(doc_id, doc['title'], doc['pageNumber'], None)) #Change maxPageNumber Later 
            doc_page_counts[doc['title']] = doc['pageNumber']
            if (max_doc_id < doc_id):
                max_doc_id = doc_id 
            tokens = doc['tokens'].lower().split(' ')
            for token in tokens:
                doc_counts = token_counts.setdefault(token, array.array('i')) #Array of counts 
                while len(doc_counts) <= doc_id:
                    doc_counts.append(0)
                doc_counts[doc_id] += 1
    documents = [
        DocumentInfo(doc.id, doc.title, doc.pageNumber, doc_page_counts[doc.title])
        for doc in documents
    ]

    #Post Processing - Because create expects all the arrays of token_counts to have same length
    for doc_counts in token_counts.values():
        while len(doc_counts) <= max_doc_id:
            doc_counts.append(0)

    #----------------------- DocumentInfo = namedtuple('DocumentInfo', ['id', 'title', 'pageNumber', 'maxPageNumber'])
    #token_counts =  # Dict of {"token1": [0, 1, 0, 3, ...], "token2": [2, 1, 5, 0, ...], ...}, all same length       
    
    DataFile.create(path, documents, token_counts)


def main(params: OverviewViewParams, output_path: Path) -> None:
    # TODO write progress reports to stdout
    generate_index(params, output_path)
    # and exit 0




if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="\n".join(
            [
                "Generate an inverted index for contents of an Overview document set.",
                "",
                "On success, return 0 and write ./[server]/[document_set_id]/[api_token].out",
                "",
                "On failure, return non-zero and write to standard error",
            ]
        )
    )
    parser.add_argument("server", help="URL to Overview server")
    parser.add_argument(
        "document_set_id", help="Overview DocumentSet ID on the server `server`"
    )
    parser.add_argument(
        "api_token",
        help="API Token that can read the document set on the Overview server.",
    )
    parser.add_argument("output_filename", help="Filename to write to.")

    args = parser.parse_args()

    params = OverviewViewParams(
        server=args.server,
        document_set_id=args.document_set_id,
        api_token=args.api_token,
    )
    output_path = Path(args.output_filename)
    main(params, output_path)
