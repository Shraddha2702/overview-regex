import React, { useState, useEffect, useCallback } from "react";
import styled from "styled-components";
import { withRouter } from "react-router-dom";
import HeatMapFill from "./HeatMapFill";
import "../Heatmap/Heatmap.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Dropdown } from "react-bootstrap";
import { scrollIntoView } from "scroll-into-view";
import ScrollContainer from "../ScrollContainer";
import { query } from "../../utils";
import axios from "axios";

import LoadingScreen from "../LoadingScreen";
import SearchNow from "../LoadingScreen/SearchNow";
import Note from "../Note";

import { preprocess } from "./functions/preprocess.js";
import { getPages, getFrequency } from "./functions/preprocessServerOutput.js";

//SAMPLE DATA
//import dataFreq from '../../data/freq.json'
//import pages from '../../data/pages.json'

const { server, apiToken, documentSetId } = query;
//const callServer = 'http://127.0.0.1:3334';
const callServer = "https://overview-react.data.caltimes.io";

//console.log(server, apiToken, documentSetId);

//Main Functional Component for Index.js
function Heatmap(props) {
  //To access the Document selected using Dropdown in the Topbar
  const [isJumpTo, setIsJumpTo] = useState("All");

  //To access the data for display
  const [data, setData] = useState(null);

  //Name of All Documents
  const [documentArray, setDocumentArray] = useState({});
  const [globalKeyword, setGlobalKeyword] = useState("");

  const [processing, setProcessing] = useState(true);

  var docs = {};

  //Get => Preprocess => Make ready for Prpeocess Function -> We get the Data

  async function fetchData(keyword) {
    try {
      axios({
        method: "GET",
        url: `${callServer}/search?documentSetId=${documentSetId}&server=${server}&term=${keyword}`,
        headers: {
          Authorization: "Basic " + btoa(apiToken + ":x-auth-token"),
        },
      }).then((keywordCall) => {
        //const keywordDivision = freq; maxPages = pages; //DEMO DATA
        var keywordDivision = keywordCall.data["foundEntries"];

        //console.log(keywordDivision);

        if (keywordDivision === undefined) {
          console.log("KEYWORD NOT FOUND IN THE DICTIONARY");
          setDocumentArray({});
          setData(null);
          setGlobalKeyword(keyword);
        } else {
          console.log("SHOWING SEARCH RESULTS");

          const maxPages = getPages(keywordDivision);
          const freq = getFrequency(keywordDivision, maxPages);

          //NEW VERSION - REMOVE LATER ON
          var returnValue = preprocess(keyword, freq, maxPages);
          setData(returnValue);

          docs["All"] = { start: 0, end: 0, title: "All" };
          docs = { ...docs, ...maxPages };
          setDocumentArray(docs);

          setGlobalKeyword(keyword);
        }
      });
    } catch (error) {
      console.error(error);
    }
  }

  //Method for Scroll to Work
  //Used by the options in the Dropdown Menu
  function scrollToTop() {
    scrollIntoView({
      top: 1,
      align: { topOffset: "150px" },
    });
  }

  //Method for Dropdown Menu
  function mouseEnterJump(event, k) {
    event.preventDefault();
    if (k === "All") {
      if (isJumpTo === k) {
        return;
      }
    } else {
      if (isJumpTo === k) {
        return;
      }
    }
    setIsJumpTo(k);

    if (k !== "All") {
      document.querySelector("#" + k).scrollIntoView(true);
      document.querySelector("div.topbar").style.height = "14%";
      window.scrollBy(0, -100);
    } else {
      document.querySelector("div.topbar").style.height = "20%";
      document.querySelector("body").scrollIntoView(scrollToTop);
    }
  }

  //Method called when the particular Notification event occurs
  const onNotifyDocumentListParams = useCallback((e) => {
    if (e.data.event === "notify:documentListParams") {
      let keyword = e.data.args[0].q;

      if (keyword === undefined) {
        //0 - - In any case show Loading Screen
        console.log("NO KEYWORD IN THE INPUT BAR");
        setDocumentArray({});
        setData(null);
        setGlobalKeyword("");
      } else {
        //1 - - Look up if the word is present in the dictionary
        console.log("INPUT FOUND");
        fetchData(keyword);
      }
    }
  });

  // When user starts scrolling down - the size of the Topbar needs to reduce
  function scrollEventListener(e) {
    if (
      document.body.scrollTop > 50 ||
      document.documentElement.scrollTop > 50
    ) {
      document.querySelector(".topbar").style.height = "14%";
    } else {
      document.querySelector(".topbar").style.height = "20%";
    }
  }

  useEffect(() => {
    axios({
      method: "POST",
      url: `${callServer}/generate`,
      data: {
        documentSetId: documentSetId,
        server: server,
      },
      headers: {
        Authorization: `Basic ${btoa(apiToken + ":x-auth-token")}`,
      },
    }).then((res) => {
      setProcessing(false);
      console.log("Generate Done");
    });
  }, []);

  // Use Effect has event Listeners for wait for message from Overview
  useEffect(() => {
    window.addEventListener(`message`, onNotifyDocumentListParams);
    window.addEventListener("scroll", scrollEventListener);

    window.parent.postMessage({ call: "notifyDocumentListParams" }, "*");

    return () =>
      window.removeEventListener(`message`, onNotifyDocumentListParams);
  }, []);

  if (data === null) {
    if (globalKeyword === "") {
      //Data is null as no Keyword present in the Input bar
      //Just show a Loading screen
      return <SearchNow processing={processing} />;
    } else {
      //Data is null as the keyword is not found - making it invalid
      return <LoadingScreen keyword={globalKeyword} />;
    }
  } else {
    return (
      <Container>
        <TopBar className="topbar">
          <Keyword>
            <Heading>
              <b>Heatmap</b>
            </Heading>
            <EachPara>
              <b>Searched terms: </b>
              {globalKeyword}
            </EachPara>
          </Keyword>

          <Options>
            <Dropdown>
              <Dropdown.Toggle style={dropdownBasic}>JUMP TO</Dropdown.Toggle>

              <Dropdown.Menu style={{ padding: "0px", textAlign: "center" }}>
                {Object.keys(documentArray).map((e, k) => {
                  if (e === isJumpTo) {
                    return (
                      <Dropdown.Item
                        key={"parent" + k}
                        href={"#" + e}
                        onClick={(event) => mouseEnterJump(event, e)}
                        style={{ padding: "0px" }}
                      >
                        <ScrollContainer
                          key={"child" + k}
                          active="true"
                          idValue={e}
                          data={documentArray[e]["title"]}
                        />
                      </Dropdown.Item>
                    );
                  } else {
                    return (
                      <Dropdown.Item
                        key={"parent" + k}
                        href={"#" + e}
                        onClick={(event) => mouseEnterJump(event, e)}
                        style={{ padding: "0px" }}
                      >
                        <ScrollContainer
                          key={"child" + k}
                          active="false"
                          idValue={e}
                          data={documentArray[e]["title"]}
                        />
                      </Dropdown.Item>
                    );
                  }
                })}
              </Dropdown.Menu>
            </Dropdown>
          </Options>
          <Note />
        </TopBar>

        <HeatMapFill data={data} />
      </Container>
    );
  }
}

const dropdownBasic = {
  backgroundColor: "#1E2229",
  borderColor: "#1E2229",
  color: "#FFFFFF",
  marginRight: "50px",
  fontSize: "14px",
  lineHeight: "15px"
};

const TopBar = styled.div`
  font-family: "Archivo", sans-serif;
  font-style: normal;
  position: fixed;
  width: 100%;
  height: 20%;
  padding-bottom: 40px;
  background: #1e2229;
  z-index: 1;
  display: flex;
  justify-content: space-between;

`;

const Keyword = styled.div`
  position: relative;
  float: left;
  padding-left: 20px;
  top: 25%;
  color: #ffffff;
`;

const Heading = styled.p`
  margin: 0px;
  margin-bottom: 5px;
  font-size: 20px;
`;

const EachPara = styled.p`
  margin: 0px;
`;

const Options = styled.div`
  position: relative;
  float: right;
  top: 50%;
  padding-right: 20px;
  color: #a0afc7;
  display: flex;
  flex-direction: row;
  justify-content: space-around;
`;

const Container = styled.div`
  background-color: #fafafb;
`;

export default withRouter(Heatmap);
