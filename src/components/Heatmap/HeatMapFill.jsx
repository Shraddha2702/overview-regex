import React from "react";
import { select, event, selectAll } from "d3-selection";
import { pairs, range } from "d3";
import { legendColor } from "d3-svg-legend";
import "d3-transition";
import { max, min } from "d3-array";
import { scaleLinear, scaleSequential } from "d3-scale";
import styled from "styled-components";
import { withRouter } from "react-router-dom";
import "./Heatmap.css";

class HeatMapFill extends React.Component {
  constructor() {
    super();
    //state to remember the Index of document which is open
    this.state = {
      indexInDocumentList: null,
    };
  }

  //For D3 Dyanamic Rendering
  createHeatmap = () => {
    //Remove any division or svg already present
    selectAll(".eachDocument").remove();
    select("div").selectAll(".tooltip").remove();
    select("div.legendmap").select("svg").remove();

    //////////////////////////////////////////////////////////////////////////////////////////////
    //INPUT PARAMETERS

    //DATA VARIABLES
    //Fetch the data passed as props {"DocumentId": {keyword, document, document_id, document, page_no, row, col}}
    var fullDocumentData = this.props.data;

    //console.log(fullDocumentData);

    //{"Document ID": {Document_name}}
    var documentNames = {};
    Object.keys(fullDocumentData).forEach((d) => {
      documentNames[d] = fullDocumentData[d][0].document;
    });

    //Get all the document Names (Keys) from the data that is passed
    var documentIds = Object.keys(fullDocumentData);

    //Flatten the Data from dictionary to List - Convert dictionary to flat list
    var flatData = documentIds.flatMap((k) => fullDocumentData[k]);

    //console.log(documentIds);

    //////////////////////////////////////////////////////////////////////////////////////////////

    //D3.js VARIABLES
    const blockSize = 50;

    //COLOR RANGE
    //Min and Max Freq for creating relative color Scale
    var freqs = flatData.map((d) => parseInt(d.freq));
    var minn = min(freqs);
    var maxx = max(freqs);

    var colorRange = ["#FFFDFC", "#D06A20"];
    var myColor1 = scaleSequential().range(colorRange).domain([minn, maxx]);

    const diff = maxx - minn;

    //Fixing margins, height and width
    var margin = { top: 50, right: 50, bottom: 50, left: 50 };
    var width = window.screen.width - margin.left - margin.right;

    /////////////////////////////////////////////////////////////////////////////////////

    //ADDING LEGEND
    var legend = select("div.legendmap")
      .append("svg")
      .attr("viewBox", "0 0 100 500");

    legend
      .append("g")
      .attr("class", "legendLinear")
      .attr("transform", "translate(0, 12.5)");

    var threshold = scaleLinear().domain([minn, maxx]).range(colorRange);

    //ADDING LEGEND STYLE - DEPENDING ON THE MIN AND MAX FREQ
    var legendStyle;
    if (diff <= 1) {
      myColor1 = scaleLinear().range(colorRange).domain([minn, maxx]);

      legendStyle = legendColor()
        .cells(2) //Change
        .shapeWidth(blockSize / 2)
        .shapeHeight(blockSize / 2)
        .ascending(true)
        .shapePadding("10")
        .labels(function (d) {
          return parseInt(d["generatedLabels"][d["i"]]);
        })
        .scale(threshold);
    } else if ((diff > 1) & (diff <= 5)) {
      legendStyle = legendColor()
        .cells(diff) //Change
        .shapeWidth(blockSize / 2)
        .shapeHeight(blockSize / 2)
        .ascending(true)
        .shapePadding("10")
        .labels(function (d) {
          return parseInt(d["generatedLabels"][d["i"]]) + " +";
        })
        .scale(threshold);
    } else {
      legendStyle = legendColor()
        .shapeWidth(blockSize / 2)
        .shapeHeight(blockSize / 2)
        .ascending(true)
        .shapePadding("10")
        .labels(function (d) {
          return parseInt(d["generatedLabels"][d["i"]]) + " +";
        })
        .scale(threshold);
    }

    legend.select(".legendLinear").call(legendStyle);

    /////////////////////////////////////////////////////////////////////////////////////

    //ADDING TOOLTIP CODE
    var tooltip = select("div.heatmap")
      .append("div")
      .style("opacity", 0)
      .attr("class", "tooltip")
      .style("background-color", "white")
      .style("border", "solid")
      .style("border-width", "2px")
      .style("border-radius", "5px")
      .style("min-width", "20%")
      .style("z-index", "0")
      .style("position", "absolute")
      .style("pointer-events", "none")
      .style("cursor", "pointer");

    /////////////////////////////////////////////////////////////////////////////////////
    //CLICK EVENT CALLED WHEN A HEATMAP BLOCK IS CLICKED
    //* OVERVIEW CURRENTLY DOESN'T SUPPORT FETCHING DOCUMENT DIRECTLY BY PAGE NO */
    //TEMPORARY SOLUTION - LOGIC SHOULD BE CHANGED WHEN OVERVIEW SUPPORT CLICK EVENT
    const clickevent = (d) => {
      //console.log(d);
      if (d.indexInDocumentList === null) {
        return;
      }

      //Logic done using goToPreviousDocument and goToNextDocument
      const messagePrevious = { call: "goToPreviousDocument" };
      const messageNext = { call: "goToNextDocument" };

      //Assuming we control Overview's indexInDocumentList
      let indexInDocumentList = this.state.indexInDocumentList;

      if (indexInDocumentList === null) {
        window.parent.postMessage(messageNext, "*");
        indexInDocumentList = 0;
      }

      while (indexInDocumentList < d.indexInDocumentList) {
        indexInDocumentList += 1;
        window.parent.postMessage(messageNext, "*");
      }

      while (indexInDocumentList > d.indexInDocumentList) {
        indexInDocumentList -= 1;
        window.parent.postMessage(messagePrevious, "*");
      }
    };

    // Three function that change the tooltip when user hover / move / leave a cell
    var mouseover = function (d) {
      if (d.freq === 0) {
        tooltip.style("opacity", 1);
        select(this)
          .style("fill", "#FAFAFB")
          .style("stroke", "#FAFAFB")
          .style("stroke-width", 2.5)
          .style("opacity", 1);
      } else {
        tooltip.style("opacity", 1);
        select(this)
          .style("stroke", "#286FD8")
          .style("stroke-width", 2.5)
          .style("opacity", 1);
      }
    };
    var mousemove = function (d) {
      if (d.document_id === "Document-Set") {
        //Condition applied when the documents are stored Document Level
        tooltip
          .style("font-size", "15px")
          .style("border", "1px solid rgba(0,0,0,0.1)")
          .style("box-shadow", "3px 3px 30px -2px rgba(0,0,0,0.1)")
          .style("left", event.pageX + 10 + "px")
          .style("top", event.pageY + 15 + "px")
          .html(
            "<p class='keyword'><span style='color:#3E5372; line-height: 20px;'>" +
              d.keyword.toUpperCase() +
              "</span> SEARCH TERM</p>\
                    <hr class='head-seperator'/>\
                    <div class='textInfoHover'><p class='textDesc'><span class='freq'>" +
              d.title +
              "</span></p> \
                    <p class='instance'>" +
              d.freq +
              " instances</p><div>"
          );
      } else {
        tooltip
          .style("font-size", "15px")
          .style("border", "1px solid rgba(0,0,0,0.1)")
          .style("box-shadow", "3px 3px 30px -2px rgba(0,0,0,0.1)")
          .style("left", event.pageX + 10 + "px")
          .style("top", event.pageY + 15 + "px")
          .html(
            "<p class='keyword'><span style='color: #3E5372;line-height: 20px;'>" +
              d.keyword.toUpperCase() +
              "</span> SEARCH TERM</p>\
                    <hr class='head-seperator'/>\
                    <div class='textInfoHover'><p class='textDesc'><span class='freq'>" +
              d.document +
              ",</span> \
                    <span class='report'>pg " +
              d.page_no +
              "</span></p> \
                    <p class='instance'>" +
              d.freq +
              " instances</p><div>"
          );
      }
    };

    var mouseleave = function (d) {
      tooltip.style("opacity", 0);
      select(this)
        .style("fill", (d) => myColor1(d.freq))
        .style("stroke-width", 1.5)
        .style("stroke", "#DEDEDE")
        .style("opacity", 1);
    };

    /////////////////////////////////////////////////////////////////////////////////////
    var column = 0;
    var row = 0;

    var maxValues = documentIds.map(function (d, i) {
      var localData = fullDocumentData[d];
      var maxRow = max(localData.map((d) => parseInt(d.row))) + 1;
      var maxCol = max(localData.map((d) => parseInt(d.col))) + 1;

      if (maxRow > row) {
        row = maxRow;
      }
      if (maxCol > column) {
        column = maxCol;
      }
      return [maxRow, maxCol];
    });

    var x = scaleLinear().domain([0, column]).range([0, width]);
    var multiplier = 60;

    var scaleY = maxValues.map(function ([maxRow, maxCol], i) {
      return scaleLinear()
        .range([0, multiplier * maxRow])
        .domain([0, maxRow]);
    });

    var scaleX = maxValues.map(function ([maxRow, maxCol], i) {
      return scaleLinear()
        .range([0, x(maxCol)])
        .domain([0, maxCol]);
    });

    var useVals = {};
    maxValues.map(function ([maxRow, maxCol], i) {
      useVals[documentIds[i]] =
        "0 0 " +
        (width + margin.left + margin.right) +
        " " +
        (multiplier * maxRow + margin.top + margin.bottom);
    });

    //console.log(documentIds);
    //console.log(fullDocumentData);

    var svgs = select("div.heatmap")
      .style("padding-top", "16%")
      .selectAll("div")
      .data(pairs(range(-1, documentIds.length + 1)))
      .join("div")
      .attr("class", "eachDocument")
      .attr("id", (d, i) => documentIds.slice(d[0], d[1]))
      .selectAll("svg")
      .data((d) => documentIds.slice(d[0], d[1]))
      .enter()
      .append("div")
      .html(
        (d) =>
          "<p><span class='title'>" +
          documentNames[d] +
          "</span>\
                <span class='pages'> (" +
          fullDocumentData[d].length +
          " pages)</span></p>"
      )
      .append("svg")
      .attr("class", (d, i) => d)
      .attr("viewBox", function (d, i) {
        return useVals[d];
      })
      .append("g")
      .attr("transform", "translate(1, 1)");

    //Adding blocks of the heatmap, by accessing row and col field
    var checkx, checky;

    svgs
      .selectAll("rect")
      .data(function (d, i) {
        var arrs = Object.values(fullDocumentData);
        checky = scaleY[i];
        checkx = scaleX[i];
        return arrs[i];
      })
      .enter()
      .append("rect")
      .attr("x", function (d) {
        return checkx(d.col);
      })
      .attr("y", function (d) {
        return checky(d.row);
      })
      .attr("rx", 3)
      .attr("ry", 3)
      .attr("width", 45)
      .attr("height", 45)
      .style("fill", (d) => myColor1(d.freq))
      .style("stroke-width", 1.5)
      .style("stroke", "#DEDEDE")
      .style("opacity", 1)
      .on("mouseover", mouseover)
      .on("mousemove", mousemove)
      .on("mouseout", mouseleave)
      .on("click", clickevent);

    //Appending Line at the end of SVG for division
    //GETTING HEIGHT VALUES TO DECIDE THE POSITON OF THE LINE
    // var heightVals = maxValues.map(([maxRow, maxCol], i) => {
    //     return multiplier * maxRow + margin.bottom
    // });

    // svgs
    //     .append("line")
    //     .attr("x1", 0)
    //     .attr("y1", (d, i) => heightVals[i])
    //     .attr("x2", width)
    //     .attr("y2", (d, i) => heightVals[i])
    //     .style("stroke", "#A0AFC7")
    //     .style("stroke-width", "1px");

    /////////////////////////////////////////////////////////////////////////////////////
  };

  //TO GET THE INDEX OF THE CURRENT OPEN DOCUMENT
  onNotifyDocument = (e) => {
    if (e.data.event === "notify:document") {
      const document = e.data.args[0];
      //console.log(document);

      if (document === null) {
        this.setState({ indexInDocumentList: null });
      } else {
        this.setState({ indexInDocumentList: document.indexInDocumentList });
      }
    }
  };

  componentDidMount() {
    window.addEventListener(`message`, this.onNotifyDocument);

    //TO KNOW THE STARTING PAGE FOR PERFORMING CLICK EVENT AND OPEN CORRECT PAGE
    window.parent.postMessage({ call: "notifyDocument" }, "*");
    this.createHeatmap();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.data !== this.props.data) {
      this.createHeatmap();
    }
  }

  componentWillUnmount() {
    //Remove Event Listener
    window.removeEventListener(`message`, this.onNotifyDocument);
  }

  render() {
    return (
      <OuterDiv>
        <HeatmapDiv className="split heatmap" />
        <LegendDiv className="split legendmap" />
      </OuterDiv>
    );
  }
}

const HeatmapDiv = styled.div`
  padding-left: 20px;
  font-size: 20px;
`;

const LegendDiv = styled.div``;

const OuterDiv = styled.div`
  padding-top: 20px;
  font-family: "Archivo", sans-serif;
  font-style: normal;
  color: #3e5372;
  z-index: -1;

  @media(max-width: 800px) {
    padding-top: 120px;
  }
`;

export default withRouter(HeatMapFill);
