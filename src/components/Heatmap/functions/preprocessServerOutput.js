  //For modifying max pages
  export function getPages(keyworddiv) {
    //{"title": {"start": 0, "end": maxx}}
    var docPages = {}
    var uniqueDocs = []
    keyworddiv.forEach((x, i) => {
      if (!uniqueDocs.includes(x.title)) {
        docPages['document-' + x.title.match(/[0-9a-zA-Z]+/g).join('-')] = { "start": 1, "end": 0 }
        docPages['document-' + x.title.match(/[0-9a-zA-Z]+/g).join('-')]["end"] = x.maxPageNumber
        docPages['document-' + x.title.match(/[0-9a-zA-Z]+/g).join('-')]["title"] = x.title
      }
    });
    return docPages
  }


  //For modifying keyword Division to fetch correct keys
  export function getFrequency(keyworddiv, pageInfo) {
    //id, title, pageNumber, count, maxPageNumber
    var docFreq = []
    keyworddiv.forEach(x => {
      docFreq.push({
        "id": 'document-' + x.title.match(/[0-9a-zA-Z]+/g).join('-'), //CREATE UNIQUE IDENTIFIER FOR DOC
        "page-id": x.id, "title": x.title, //x.id is identifier for PAGE - CAN'T BE USED FOR PAGE ID
        "pageNumber": x.pageNumber, "count": x.count
      })
    });
    return docFreq
  }