/*
  Method: Preprocessing Data Pipeline
  Inputs:
    keyword: Keyword for the Heatmap
    dataFreq: Array of Objects, with each object containing 
        page-wise frequency distribution
    dataPages: Object, with Document Name as Key,
        and providing the max no. of pages in the document
  Output: Preprocessed data with row and col for Heatmap visualization
*/

//id, title, pageNumber, count, maxPageNumber
export function preprocess(keyword, dataFreq, dataPages) {

  //dictionary to access Frequency using Doc Name and Page No
  var doclevel = {};

  //console.log(dataFreq);

  if (dataFreq[0]['pageNumber'] === null) {
    //It means it is a docset
    let newDataFreq = [];

    var size = dataFreq.length;

    [...Array(size).keys()].map((i) => {
      var division = {};

      division['count'] = dataFreq[i]['count'];
      division['id'] = "Document-Set";
      division['maxPageNumber'] = size;
      division['pageNumber'] = i + 1;
      division['title'] = dataFreq[i]['title'];

      newDataFreq.push(division);
    });

    dataFreq = newDataFreq;
    dataPages = {"Document-Set": {"start": 1, "end": size, "title": "Document Set"}};

  }


  //console.log(dataFreq);
  //console.log(dataPages);

  dataFreq.forEach(function (each, i) {
    if (doclevel[each['id']] !== undefined) {
      doclevel[each['id']][each['pageNumber']] = [each['count'], each['title']];
    }
    else {
      doclevel[each['id']] = {}
      doclevel[each['id']][each['pageNumber']] = [each['count'], each['title']];
    }
  });

  //console.log(doclevel);
  //console.log(dataPages);
  
  var output = {}
  var nextIndexInDocumentList = 0;
  //console.log(doclevel);
  //OUTPUT -> keyword, document, document_id, document, page_no, row, col
  //Outer loop to go through each Document
  for (var key in dataPages) {
    output[key] = [];

    //Inner Loop to go through each page of the document 
    for (var i = 1; i <= dataPages[key]['end']; i++) {
      //console.log(dataFreq[i])
      var temp;

      //Condition applies if the document has one or more instances
      if (doclevel[key] !== undefined) {
        temp = {};
        temp['keyword'] = keyword;
        temp['document_id'] = key;
        temp['document'] = dataPages[key]['title'];
        temp['page_no'] = i;
        temp['row'] = parseInt((i - 1) / 25);
        temp['col'] = parseInt((i - 1) % 25);


        //Condition applies if the ith page is present in the DocLevel 
        //- which means it has instances
        if (doclevel[key][i]) {
          temp['freq'] = doclevel[key][i][0];
          temp['title'] = doclevel[key][i][1];
          temp['indexInDocumentList'] = nextIndexInDocumentList;
          nextIndexInDocumentList += 1;
        }

        //Condition applies if the ith page is not present in the Doclevel 
        //- means it doesn't have any instances
        else {
          temp['freq'] = 0;
          temp['title'] = null;
          temp['indexInDocumentList'] = null;
        }
        output[key].push(temp);
      }

      //Condition applies when the Document has no instances of the Keyword
      else {
        temp = {};
        temp['keyword'] = keyword;
        temp['document_id'] = key;
        temp['document'] = dataPages[key]['title'];
        temp['page_no'] = i;
        temp['row'] = parseInt((i - 1) / 25);
        temp['col'] = parseInt((i - 1) % 25);
        temp['freq'] = 0;
        temp['title'] = null;
        temp['indexInDocumentList'] = null;
        output[key].push(temp);
      }
    }
  }

  //console.log(output);
  return output;
}