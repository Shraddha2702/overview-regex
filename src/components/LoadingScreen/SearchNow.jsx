import React from "react";
import styled from "styled-components";
import Note from "../Note";

const SearchNow = ({ loading }) => (
  <div>
    <TopBar className="topbar">
      <Keyword>
        <Heading>
          <b>Heatmap</b>
        </Heading>
        <EachPara>
          <b>
            Enter your search term into the search bar above to see the heatmap.
          </b>
        </EachPara>
      </Keyword>
      <Note />
    </TopBar>
    {loading ? (
      <Message>Processing</Message>
    ) : (
      <Message>Start Searching Now</Message>
    )}
  </div>
);

const NoteContainer = styled(Note)`
  padding-top: 220px;
`;
const Message = styled.h3`
  padding-top: 220px;
  font-family: "Archivo", sans-serif;
  text-align: center;
  font-weight: bold;
  color: #a0afc7;
`;
const TopBar = styled.div`
  font-family: "Archivo", sans-serif;
  font-style: normal;
  position: fixed;
  width: 100%;
  padding-bottom: 45px;
  padding-top: 45px;
  background: #1e2229;
  z-index: 1;
`;

const EachPara = styled.p`
  margin: 0px;
`;

const Keyword = styled.div`
  position: relative;
  float: left;
  padding-left: 20px;
  top: 25%;
  color: #ffffff;
`;

const Heading = styled.p`
  margin: 0px;
  margin-bottom: 5px;
  font-size: 20px;
`;
export default SearchNow;
