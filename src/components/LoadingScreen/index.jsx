import React from 'react';
import styled from 'styled-components';

const LoadingScreen = (keyword) => {
  return (
    <StyledLoadComplete>

      <StyledImgComplete>
        <Bar><img src='/searchbar-success.png' alt="" /></Bar>
      </StyledImgComplete>
      <StyledLoadHeader>
        {
          [keyword].map((e) => {
            if (e.keyword === '') {
              return (
              <>
                <StyledLoadingHeader>Start Searching Now</StyledLoadingHeader>
                <StyledSubHeader>
                  Enter your search term into the search bar above to see the heatmap.
                </StyledSubHeader>
              </>
              )
            }
            else {
              //console.log(e['keyword']);
              return (
                <>
                  <StyledLoadingHeader>Start Searching for another Word</StyledLoadingHeader>
                  <StyledSubHeader>
                    "{e['keyword'].toUpperCase()}" Not Found
                  </StyledSubHeader>
                </>
                )
            }
          })
        }
        
      </StyledLoadHeader>
    </StyledLoadComplete>
  );
};

const StyledHeaderSection = styled.header`
  width: 100%;
  max-width: 300px;`;


const StyledLoadComplete = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 75px;
  padding-bottom: 25px;
  background-color: #3e5372;
`;

const StyledHeader = styled.h1`
  font-family: 'Archivo', sans-serif; 
  font-style: normal;
  font-weight: bold;
  font-size: 21px;
  line-height: 26px;
  text-transform: capitalize;
  color: #172d3b;
`;


const StyledImgComplete = styled.div`
    display: flex;
    flex-direction: row;
    margin-bottom: 20px;
`;

const Bar = styled.div`
`;

// const Pointer = styled.div`
//     margin-top: -30px;
//     padding: 5px
// `;


const StyledSubHeader = styled.p`
  font-family: 'Archivo', sans-serif; 
  font-style: normal;
  font-weight: bold;
  font-size: 11px;
  line-height: 125%;
  /* or 14px */
  text-align: center;
  /* or 130% */
  color: #a0afc7;
`;

const StyledLoadHeader = styled(StyledHeaderSection)`
  text-align: center;
  h1 {
    margin-bottom: 10px;
  }
  p {
    margin-bottom: 10px;
  }
`;


const StyledLoadingHeader = styled(StyledHeader)`
  font-family: 'Archivo', sans-serif; 
  font-style: normal;
  font-weight: bold;
  font-size: 21px;
  line-height: 26px;
  text-transform: capitalize;
  color: #ffffff;
`;

export default LoadingScreen;