import React from 'react';
import styled from 'styled-components';
import 'bootstrap/dist/css/bootstrap.min.css';

function ScrollContainer(data) {
    const displayText = data["data"];
    const active = data["active"];
    const idValue = data["idValue"];

    function mouseHoverEnter(event, k) {
        event.preventDefault();
        //console.log('Mouse Entered Hover');

        //Change the Color of the Vertical Bar on the right on Hover
        document.querySelector('[href="#'+k+'"] div div div').style.backgroundColor = '#F0F6FF';

        //Change the Background color of the division containing document name
        document.querySelectorAll('[href="#'+k+'"] div')[1].style.backgroundColor = '#F0F6FF';

        //Change the Font Color of the Document Name
        document.querySelectorAll('[href="#'+k+'"] p')[1].style.color = '#3E5372';
      }
    
      function mouseHoverExit(event, k) {
        event.preventDefault();
        //console.log('Mouse Exitted Hover');

        //Change the Color of the Vertical Bar on the right on Hover
        document.querySelector('[href="#'+k+'"] div div div').style.backgroundColor = '#FFFFFF';

        //Change the Background color of the division containing document name
        document.querySelectorAll('[href="#'+k+'"] div')[1].style.backgroundColor = '#FFFFFF';

        //Change the Font Color of the Document Name
        document.querySelectorAll('[href="#'+k+'"] p')[1].style.color = '#A0AFC7';
      }

  return (
      <div>
        {

            [active].map((v, i) => {
                if (v === 'false') {
                    return (<Container key={idValue}
                            onMouseEnter={event => mouseHoverEnter(event, idValue)}
                            onMouseLeave={event => mouseHoverExit(event, idValue)}>
                                <VerticalBar />
                                <Dot />
                                <TextContainer><Text>{ displayText }</Text></TextContainer>
                            </Container>)
                }
                else {
                    return (
                        <ActiveContainer key={idValue}>
                            <ActiveVerticalBar />
                            <ActiveDot />
                            <ActiveTextContainer><ActiveText>{ displayText }</ActiveText></ActiveTextContainer>
                        </ActiveContainer>)
                }
            })
        }
        </div>
  );
}


const Dot = styled.p`
    height: 5px;
    width: 6px;
    background-color: #D9E8FF;
    border-radius: 50%;
    display: inline-block;
    margin: 13px;
    margin-top: 8.2%;
`;

const ActiveDot = styled.p`
    height: 5px;
    width: 6px;
    background-color: #286FD8;
    border-radius: 50%;
    display: inline-block;
    margin: 13px;
    margin-top: 8.2%;
`;

const ActiveText = styled.p`
    width: 100%;
    font-family: 'Archivo', sans-serif;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 15px;
    color: #3E5372
`;

const Text = styled.p`
    width: 100%;
    font-family: 'Archivo', sans-serif;
    font-style: normal;
    font-weight: bold;
    font-size: 14px;
    line-height: 15px;
    color: #A0AFC7
`;

const Container = styled.div`
  padding-right: 20px;
  height: 40px;
  display: flex;
  flex-direction: row;
  background-color: #FFFFFF;
  border-bottom: 0.5px solid #D9E8FF;
`;

const ActiveContainer = styled.div`
    padding-right: 20px;
    height: 40px;
    display: flex;
    flex-direction: row;
    background-color: #D9E8FF;
`;

const ActiveVerticalBar = styled.div`
    width: 5px;
    background-color: #286FD8;
`;

const VerticalBar = styled.div`
    width: 5px;
    background-color: #FFFFFF;
`;

const ActiveTextContainer = styled.div`
    width: 100%;
    margin-top: 5.6%;
`;

const TextContainer = styled.div`
    width: 100%;
    margin-top: 5.6%;
`;

export default ScrollContainer;
