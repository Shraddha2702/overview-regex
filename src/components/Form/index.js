import React from 'react';
import styled from 'styled-components';
import 'bootstrap/dist/css/bootstrap.min.css';

const Span  = styled.span`
  color: #666;
  font-family: monospace;
  font-size: 11px;
  white-space: pre;
`;

const Input = styled.input`
  width: 95%;
  font-size: 11px;
  padding: 5px;
  border-radius: 1px;
  border: 1px solid #676767;
`

const InfoHelper = styled.p`
  font-size: 11px
`;

const TitleHelper = styled.p`
  font-size: 16px
`;

const Title = styled.td`
  width: 30%
`;

const TextInput = styled.td`
  width: 30%
`;

const Extra = styled.td`
width: 40%
`;

const Container = styled.div`
  padding: 10px;
`;

function Form() {
  return (
    <Container>
      <form>
        <table class="table table-hover table-responsive">
          <tbody>
          <b>Lookup for Documents with...</b>
            {/* <tr>
              <Title><TitleHelper>for any word</TitleHelper></Title>
              <TextInput><Input placeholder="Smith"  /></TextInput>
              <Extra><InfoHelper>Normal search text</InfoHelper></Extra>
            </tr> */}
            <tr>
            <Title><TitleHelper>for any word BUT is case sensitive</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT" placeholder="/Smith/"  /></TextInput>
              <Extra><InfoHelper>Type <Span>/Smith/</Span> to search for all occurances of "Smith" - Case sensitive</InfoHelper></Extra>
            </tr>
            {/* <tr>
              <Title><TitleHelper>for any word BUT is case in-sensitive</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT" placeholder="/(?i)Smith/"  /></TextInput>
              <Extra><InfoHelper>Type <Span>/(?i)Smith/</Span> to search for all occurances of "Smith" - Case in-sensitive</InfoHelper></Extra>
            </tr>
            <tr>
              <Title><TitleHelper>similar to previous search, but Faster</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="text:Smith AND text:/Smith/"  /></TextInput>
              <Extra><InfoHelper>Type <Span>text:Smith AND text:/Smith/</Span> to search the index for the word smith, then scan the matching documents for the correct capitalization</InfoHelper></Extra>
            </tr>

            <tr>
              <Title><TitleHelper>for any word with one or more letters replacable</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="/caf[ée]/"  /></TextInput>
              <Extra><InfoHelper>Type <Span>/caf[ée]/</Span> to search for the sequence: c, a, f, and then either e or é</InfoHelper></Extra>
            </tr>
            <tr>
              <Title><TitleHelper>for any word in a line with specific pattern</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="/(?m)^-- \nAdam/"  /></TextInput>
              <Extra><InfoHelper>Type <Span>/(?m)^-- \nAdam/</Span> to search for a line starting with two dashes, a space, a newline, and then the letters Adam</InfoHelper></Extra>
            </tr>
            
            <tr>
              <Title><TitleHelper>phrases</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="John Smith"  /></TextInput>
              <Extra><InfoHelper>Type <Span>John Smith</Span> to search for all documents containing phrase John Smith</InfoHelper></Extra>
            </tr> */}
            <tr>
              <Title><TitleHelper>multiple phrases occuring in one document</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="John Smith AND Alice Smith"  /></TextInput>
              <Extra><InfoHelper>Type <Span>John Smith AND Alice Smith</Span> to search for all documents containing both the phrase “John Smith” and the phrase “Alice Smith“</InfoHelper></Extra>
            </tr>
            <tr>
              <Title><TitleHelper>optional occurence of multiple phrases in one document</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="John Smith OR Alice Smith"  /></TextInput>
              <Extra><InfoHelper>Type <Span>John Smith OR Alice Smith</Span> to search for all documents containing both the phrase “John Smith” or the phrase “Alice Smith“</InfoHelper></Extra>
            </tr>
            <tr>
              <Title><TitleHelper>one phrase and exludes the other phrase</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="John Smith AND NOT Alice Smith"  /></TextInput>
              <Extra><InfoHelper>Type <Span>John Smith AND NOT Alice Smith</Span> to search for all documents containing both the phrase “John Smith” and not the phrase “Alice Smith“</InfoHelper></Extra>
            </tr>
            {/* <tr>
              <Title><TitleHelper>when you have more than 2 arguments for the previous case</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="Alice AND NOT (Bob OR Carol)"  /></TextInput>
              <Extra><InfoHelper>Type <Span>Alice AND NOT (Bob OR Carol)</Span> to search for all documents containing the phrase “Alice” and neither the phrase “Bob” nor the phrase “Carol“</InfoHelper></Extra>
            </tr>
            <tr>
              <Title><TitleHelper>you want to look for a phrase containing "AND / OR"</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder='"John and Alice Smith"'  /></TextInput>
              <Extra><InfoHelper>Type <Span>John and Alice Smith</Span> to search for all documents containing the phrase “John and Alice Smith“</InfoHelper></Extra>
            </tr> */}


            <tr>
              <Title><TitleHelper>two words than for x words apart</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="John Smith~2"  /></TextInput>
              <Extra><InfoHelper>Type <Span>John Smith~2</Span> to search for all documents matching the phrase “John Smith” or phrases with the words John and Smith at most 2 words apart, such as “John 'The Culprit' Smith“</InfoHelper></Extra>
            </tr>
            {/* <tr>
              <Title><TitleHelper>words beginning with</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="Smith*"  /></TextInput>
              <Extra><InfoHelper>Type <Span>Smith*</Span> to search for all documents containing a word that begins with “Smith“, such as  “Smith“, “Smithy”</InfoHelper></Extra>
            </tr>
            <tr>
              <Title><TitleHelper>word whose spelling you aren't sure about</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="Pizza~"  /></TextInput>
              <Extra><InfoHelper>Type <Span>Pizza~</Span> to search for all documents matching the word “Pizza” or similar words such as “Piazza” or “Pizzas“</InfoHelper></Extra>
            </tr>
            
            <br/><br/>
            <b>Search Documents by name ...</b>
            <tr>
              <Title><TitleHelper>title</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="title:John Smith"  /></TextInput>
              <Extra><InfoHelper>Type <Span>title:John Smith</Span> to search for all documents containing the phrase “John Smith” in their titles</InfoHelper></Extra>
            </tr>
            <tr>
              <Title><TitleHelper>for files with this File Path</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="/path\/to\/file\.txt/"  /></TextInput>
              <Extra><InfoHelper>Type <Span>/path\/to\/file\.txt/</Span> to search for file with their filepath</InfoHelper></Extra>
            </tr>
            <tr>
              <Title><TitleHelper>for files with this File Name</TitleHelper></Title>
              <TextInput><Input TYPE="TEXT"  placeholder="title:/file\.txt/"  /></TextInput>
              <Extra><InfoHelper>Type <Span>title:/file\.txt/</Span> to search for file with it's title as 'file.txt'</InfoHelper></Extra>
            </tr> */}
            </tbody>
        </table>
        <div><button type="submit" class="btn btn-primary">Submit</button></div>
      </form>
    </Container>
  );
}

export default Form;
