import React from "react";
import styled from "styled-components";

const Note = () => (
  <StyledContainer>
    <div>
      <svg
        width="11"
        height="10"
        viewBox="0 0 11 10"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M5.50001 4V5.49997M5.50001 6.99994H5.50505M2.00927 9H8.99073C9.76667 9 10.2514 8.16649 9.86342 7.49998L6.37268 1.49988C5.98471 0.833372 5.01529 0.833372 4.62732 1.49988L1.13658 7.49998C0.748613 8.16649 1.23333 9 2.00927 9Z"
          stroke="black"
          stroke-linecap="round"
          stroke-linejoin="round"
        />
      </svg>
    </div>
    <p>
      <span class="note">NOTE: </span>
      This feature is still in beta, and we're continuing to make updates. If
      you come across any bugs, please email us at{" "}
      <a href="mailto:max.lu@latimes.com" class="email">
        max.lu@latimes.com
      </a>
      .
    </p>
  </StyledContainer>
);

const StyledContainer = styled.div`
  background: #ffd644;
  position: absolute;
  display: flex;
  width: 100%;
  padding-left: 50px;
  padding-right: 10px;
  padding: 5px 10px 5px 50px;
  align-items: flex-start;
  text-align: center;
  bottom: -16px;

  @media(max-width: 750px) {
    bottom: -40px;
  }

  @media(max-width: 420px) {
    bottom: -55px;
  }
  @media(max-width: 320px) {
    bottom: -70px;
  }
  div {
    display: flex;
    justify-content: center;
    align-items: center;
    padding-right: 10px;
    position: relative;
    top: 3px;
  }

  .note {
    font-family: "Archivo", sans-serif;
    font-weight: bold;
    font-size: 11px;
  }

  p {
    font-family: "Archivo", sans-serif;
    font-size: 11px;
    margin-bottom: 0;
    text-align: left;
  }

  .email {
    text-decoration: underline;
    color: black;
  }
`;

export default Note;
