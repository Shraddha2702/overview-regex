import axios from 'axios';

/**
 * set baseurl and authentication headers with axios
 *
 * @param {string} apiToken token taken from useQuery sent by overview
 */

//const url = 'https://overview-react.data.caltimes.io';
const url = 'http://127.0.0.1:3334'

function AxiosWithAuth(apiToken) {
  return axios.create({
    headers: {
      Authorization: `Basic ${btoa(apiToken + ':x-auth-token')}`
    },
    baseURL: url
  });
}

export default AxiosWithAuth;