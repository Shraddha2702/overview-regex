
const search = JSON.parse(
    '{"' +
    decodeURI(window.location.search.substring(1))
        .replace(/"/g, '\\"')
        .replace(/&/g, '","')
        .replace(/=/g, '":"') +
    '"}'
);
const server = decodeURIComponent(search.server);
const apiToken = decodeURIComponent(search.apiToken);
const documentSetId = decodeURIComponent(search.documentSetId);
const query = {
    server,
    apiToken,
    documentSetId
};

export default query;