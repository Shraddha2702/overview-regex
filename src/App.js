import React from 'react';
import './App.css';
//import Form from './components/Form';
import Heatmap from './components/Heatmap';
//import BarChartData from './components/BarChartData'
import { Route } from 'react-router-dom';

function App() {
  return (
    <Route path="/show"><Heatmap /></Route>
  );
}

export default App;
