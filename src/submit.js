const submit = () => {
    var word1 = document.getElementById("word1").value.toLowerCase();
    var word2 = document.getElementById("word2").value.toLowerCase();
    var apart = document.getElementById("apart").value.toLowerCase();

    var word1_camel = word1.slice(0, 1).toUpperCase() + word1.slice(1, );
    var word2_camel = word2.slice(0, 1).toUpperCase() + word2.slice(1, );

    // document.getElementById('display').innerHTML = 
    //     "/\\b"+'('+`${word1}`+'|'+`${word1_camel}`+')'+'\\W+(?:\\w+\\W+){1,'+`${apart}`+'}?'
    //         +'('+`${word2}`+'|'+`${word2_camel}`+')'+"\\b|"+
    //     "\\b"+'('+`${word2}`+'|'+`${word2_camel}`+')'+'\\W+(?:\\w+\\W+){1,'+`${apart}`+'}?'
    //     +'('+`${word1}`+'|'+`${word1_camel}`+')'+"\\b/";

    document.getElementById('display').innerHTML = 
    `/\\b(?:(${word1}|${word1_camel})\\W+(?:\\w+\\W+){1,${apart}}?(${word2}|${word2_camel})|(${word2}|${word2_camel})\\W+(?:\\w+\\W+){1,${apart}}?(${word1}|${word1_camel}))\\b/`;
} 

export default submit;