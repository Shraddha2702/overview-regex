The project is made using React. Run the below command to start the web app.       

## Command to run App   
**npm start**        

## Components involved:    
1. Loading Screen   
    - It will be displayed as soon as one open the heatmap App. 
    - It stays on the screen until all the data points are generated and available for Heatmap to display. 
    - It will also appear when a certain Keyword in the search Bar does not appear in the Documents.    
            
2. Heatmap
    - Once we have all the data points ready, the Heatmap is loaded.
    - Heatmap involves a Topbar with the current Keyword displayed on the Left and a dropdown Option with all the document names on the right hand side. 
    - Following the Topbar is the Heatmap view, with individual heatmap for each document. The dropdown option in the Topbar can be used to jump to a particular document.
    - Hovering over each page shows the frequency of the heatmap on that page of the document. 
     
     
## Technical Details

1. File -> src/components/Heatmap/index.js (Functional Component)

   - useEffect() -> Event Listener for notifyDocumentListParams from Overview

   - function -> preprocess(keyword, dataFreq, dataPages) [src/Heatmap/functions/preprocess.js]
        - Location: src/components/Heatmap/index.js
        - Method: Preprocessing Data Pipeline
        - Inputs:
            - keyword: Keyword for the Heatmap (String - '')    
            - dataFreq: Array of Objects, with each object containing page-wise frequency distribution      
                [{"dname": "Mueller-Report", "pn": 89, "fq": 1}, {"dname": "Mueller-Report-1", "pn": 89, "fq": 1}, {}, {}, .]

            - dataPages: Object, with Document Name as Key, and providing the max no. of pages in the document     
                { "Document Id": { "start": 0, "end": #max-page-no },      
                "Mueller-Report": { "start": 0, "end": 488 } , . . . }      

        - Output: Preprocessed data with row and col for Heatmap visualization     
            {"Document id": [ {keyword: '', document: '', pageno: '', row: '', col: ''}, {}, {} ], }     
            Object with Key as DocumentId, and Value as an array of objects with each element giving frequency of word on that page.    

   - function -> fetchData(url, keyword, docset) 
       - Fetch the data for Keyword frequency for document and page wise    
       - If no data is found then set data as null and it is appropriately handled in the UI
       - Otherwise, Fetch the data, and send the Keyword, Keyword Frequency Distribution, and max Pages to Preprocess Function
     
      
     
2. File -> src/components/Heatmap/HeatMapFill.jsx (Class Component)

   - function -> createHeatmap()
     - Setting up axis, Scale, Legends, height, width, tooltip and creating blocks of the Heatmap - all defined in this function.
        
   - function -> clickevent (d) 
     - Performs window.postMessage to loop through the document pages until correct page is open 
     
     
    
## FLOW
useEffect index.js -> on Event notifyDocumentListParams -> Calls Method onNotifyDocumentListParams     
-> Keyword found -> FetchData Method called      
-> Data, DocumentArray, globalKeyword Set (preprocess Method internally called)      
-> once the new States are set, the view is re-rendered      
-> HeatmapFill Component Called with all the data points      
-> ComponentDidMount waits for User to put a keyword in the Search bar / Fetches keyword from the bar if already present     
-> Once fetched the keyword - createHeatmap() function called      
-> ComponentDidUpdate called everytime the data passed by the parent differs from the old data     
-> ComponentDidUpdate calls createHeatmap() function with new values set     
   
PS: Refer Flowchart in documentation for more information.     

     
## To Run it locally 
1. cd into that server folder (Lookup for server_deployment_heatmap Folder in this Project)               
     
2. Terminal 1: Run Flask Server: python3 deploy.py       
              
3. Terminal 2: Generate Inverted Index       
curl http://127.0.0.1:3334/generate --user <plugin-api-token>:x-auth-token --data "documentSetId=4&server=http://localhost:9000"
         
          
4. Search for a Keyword in the Inverted Index        
curl "http://127.0.0.1:3334/search?documentSetId=3&server=http://localhost:9000&term=data" --user <plugin-api-token>:x-auth-token    
        
           
            
## Overview API Calls
          
curl https://overview-react.data.caltimes.io/generate --user <plugin-api-token>:x-auth-token --data "documentSetId=21049&server=https://overviewdocs.com"    
      
      
curl "https://overview-react.data.caltimes.io/search?documentSetId=21049&server=https://overviewdocs.com&term=trump" --user <plugin-api-token>:x-auth-token    
    
       
         
## D3 Heatmap Tutorial
    
- https://www.d3-graph-gallery.com/graph/heatmap_basic.html       
- https://www.d3-graph-gallery.com/graph/heatmap_style.html